import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { Button } from "react-bootstrap";
import Typography from "@mui/material/Typography";
import { useHistory } from "react-router-dom";
import googleLogo from "../../img/fb-logo.png";
import { useSelector, useDispatch } from "react-redux";
import { shoppingCartAction } from "../../redux/action/shoppingCartAction";
import log from "loglevel";
import { NotificationUtil } from "../../util/NotificationUtil";
import { ITEM_ADDED_TO_CART_SUCCES } from "../../constants/constants";
import { Fab } from "@mui/material";
import { Add } from "@mui/icons-material";

function ProductCard(props) {
  const history = useHistory();
  const user = useSelector((state) => {
    return state.userReducer.user;
  });
  const dispatch = useDispatch();

  function handleBuyClick() {
    log.info(
      "UserId: %d. Product with ID: %d has been added to Shopping cart.",
      user.id,
      props.product.id
    );
    dispatch(shoppingCartAction.addToCart(props.product));
    NotificationUtil.success(ITEM_ADDED_TO_CART_SUCCES);
  }

  function redirectToDetail() {
    history.push({
      pathname: "/productDetail",
      state: props.product,
    });
  }

  return (
    <Card>
      <CardMedia
        component="img"
        height="140"
        image={googleLogo}
        alt="Image not found"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.product.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {props.product.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Fab
          color="primary"
          aria-label="add"
          size="small"
          onClick={handleBuyClick}
        >
          <Add />
        </Fab>
        <Button size="small" variant="secondary" onClick={redirectToDetail}>
          Details
        </Button>
      </CardActions>
    </Card>
  );
}

export default ProductCard;
