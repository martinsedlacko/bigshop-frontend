import Home from "../view/Home/Home";
import { Switch, Route } from "react-router-dom";
import React, { Component } from "react";
import PrivateRoute from "./common/PrivateRoute";
import Profile from "../view/Profile/Profile";
import Login from "../view/Login/Login";
import OAuth2RedirectHandler from "../service/OAuth2RedirectHandler";
import NotFound from "./NotFound/NotFound";
import ProductView from "../view/ProductView/ProductView";
import ProductDetail from "../view/ProductDetail/ProductDetail";
import ShoppingCart from "../view/ShoppingCart/ShoppingCart";

class Routes extends Component {
  render() {
    return (
      <Switch>
        <PrivateRoute
          exact
          path="/"
          component={Home}
          authenticated={this.props.authData.authenticated}
          currentUser={this.props.authData.authenticated}
        />
        <PrivateRoute
          path="/profile"
          component={Profile}
          authenticated={this.props.authData.authenticated}
          currentUser={this.props.authData.authenticated}
          {...this.props}
        />
        <Route
          path="/login"
          render={(props) => (
            <Login
              authenticated={this.props.authData.authenticated}
              {...props}
            />
          )}
        ></Route>
        <Route
          path="/oauth2/redirect"
          component={OAuth2RedirectHandler}
        ></Route>
        <PrivateRoute
          path="/products"
          component={ProductView}
          authenticated={this.props.authData.authenticated}
          currentUser={this.props.authData.authenticated}
        />
        <PrivateRoute
          path="/productDetail"
          component={ProductDetail}
          authenticated={this.props.authData.authenticated}
          currentUser={this.props.authData.authenticated}
        />
        <PrivateRoute
          path="/shoppingCart"
          component={ShoppingCart}
          authenticated={this.props.authData.authenticated}
          currentUser={this.props.authData.authenticated}
        />
        <Route component={NotFound}></Route>
      </Switch>
    );
  }
}

export default Routes;
