import React from "react";
import { useSelector } from "react-redux";

function Header() {
  const header = useSelector((state) => state.header.value);
  return <header className="App-header">{header}</header>;
}

export default Header;
