import React from "react";
import Header from "../Header/Header";
import Navigation from "../Navigation/Navigation";
import { Row, Col } from "react-bootstrap";

function Layout({ children, authData, logOut }) {
  if (authData.authenticated) {
    return (
      <React.Fragment>
        <Row>
          <Col>
            <Header />
          </Col>
        </Row>
        <Row>
          <Col xs={2}>
            <Navigation logOut={logOut} authData={authData} />
          </Col>
          <Col xs={9} className="pt-5">
            <main>{children}</main>
          </Col>
        </Row>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <main>{children}</main>
      </React.Fragment>
    );
  }
}

export default Layout;
