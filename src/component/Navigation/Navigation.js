import React from "react";
import { Link } from "react-router-dom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import AppBar from "@mui/material/AppBar";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import Header from "../Header/Header";

const drawerWidth = "15%";

function Navigation(props) {
  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
        <Header />
      </AppBar>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth,
            boxSizing: "border-box",
            backgroundColor: "#3f3f3f",
          },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: "auto" }} className="pt-5">
          <List>
            <ListItem button key="shoppingCart">
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <Link to="/shoppingCart" className="App-link">
                Shopping cart
              </Link>
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button key="home">
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <Link to="/" className="App-link">
                Home
              </Link>
            </ListItem>
            <ListItem button key="profile">
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <Link to="/profile" className="App-link">
                Profile
              </Link>
            </ListItem>
            <ListItem button key="productView">
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <Link to="/products" className="App-link">
                Products
              </Link>
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button key="login">
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              {props.authData.authenticated ? (
                <a href="/login" onClick={props.logOut} className="App-link">
                  Logout
                </a>
              ) : (
                <Link to="/login" className="App-link">
                  Login
                </Link>
              )}
            </ListItem>
          </List>
        </Box>
      </Drawer>
    </Box>
  );
}

export default Navigation;
