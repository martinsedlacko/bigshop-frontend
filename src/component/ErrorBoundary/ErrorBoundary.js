import React from "react";
import log from "loglevel";
import remote from "loglevel-plugin-remote";
import { LOGGER_URL } from "../../constants";

const customJSON = (log) => {
  return {
    message: log.message,
    level: log.level.label,
    stacktrace: log.stacktrace,
  };
};

const options = {
  url: LOGGER_URL,
  format: customJSON,
  method: "POST",
};

remote.apply(log, options);

log.enableAll();

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  static getDerivedStateFromError(error) {
    return {
      hasError: true,
    };
  }

  componentDidCatch(error, info) {
    log.error(error.message);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong ...</h1>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
