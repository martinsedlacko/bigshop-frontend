import React from "react";
import { Container } from "react-bootstrap";

function ContentWithBorder({ children }) {
  return <Container className="with-border">{children}</Container>;
}

export default ContentWithBorder;
