import React from "react";
import { Col } from "react-bootstrap";

function ErrorView(props) {
  return (
    <Col sm={{ size: 4, offset: 4 }}>
      <div className="pt-5 "> Oops, something went wrong...</div>
      <div className="pt-2">Error message: {props.message}</div>
      <div className="pt-2">Error status: {props.status}</div>
    </Col>
  );
}

export default ErrorView;
