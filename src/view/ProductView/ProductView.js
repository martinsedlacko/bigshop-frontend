import React, { useEffect, useState } from "react";
import { productService } from "../../service/ProductService";
import { useDispatch } from "react-redux";
import { setHeader } from "../../redux/reducer/headerReducer";
import ProductCard from "../../component/ProductCard/ProductCard";
import ErrorView from "../ErrorView/ErrorView";
import { Grid } from "@mui/material";
import Content from "../../component/Content/Content";

function ProductView() {
  const dispatch = useDispatch();
  const [products, setProducts] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    let isMounted = true;
    dispatch(setHeader("Product View"));

    async function fetchProducts() {
      try {
        let response = await productService.getAllProducts();
        if (isMounted) setProducts(response);
      } catch (error) {
        if (isMounted) setError(error);
      }
    }
    fetchProducts();

    return () => {
      isMounted = false;
    };
  }, [dispatch]);

  if (error) {
    return <ErrorView message={error.error} status={error.status}></ErrorView>;
  } else
    return (
      <Content className="text-center pb-3">
        {error !== null && console.log(error)}
        <Grid container spacing={2} className="pt-3">
          {products !== null &&
            products.map((product) => {
              return (
                <Grid item xs={8} md={4} lg={3} key={product.id}>
                  <ProductCard key={product.id} product={product} />
                </Grid>
              );
            })}
        </Grid>
      </Content>
    );
}

export default ProductView;
