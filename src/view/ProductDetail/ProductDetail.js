import { CardActions } from "@mui/material";
import React, { useEffect } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import googleLogo from "../../img/google-logo.png";
import { useHistory } from "react-router-dom";
import log from "loglevel";
import { useDispatch, useSelector } from "react-redux";
import { shoppingCartAction } from "../../redux/action/shoppingCartAction";
import { NotificationUtil } from "../../util/NotificationUtil";
import { ITEM_ADDED_TO_CART_SUCCES } from "../../constants/constants";
import { Fab } from "@mui/material";
import { Add } from "@mui/icons-material";
import { setHeader } from "../../redux/reducer/headerReducer";

function ProductDetail(props) {
  const history = useHistory();
  const product = props.location.state;
  const user = useSelector((state) => {
    return state.userReducer.user;
  });
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setHeader("Home"));
  }, [dispatch]);

  function handleBuyClick() {
    log.info(
      "UserId: %d. Product with ID: %d has been added to Shopping cart.",
      user.id,
      product.id
    );
    dispatch(shoppingCartAction.addToCart(product));
    NotificationUtil.success(ITEM_ADDED_TO_CART_SUCCES);
  }

  return (
    <div className="pt-5">
      <Row>
        <Col xs={{ span: 4, offset: 4 }}>
          <Card className="pb-2">
            <Card.Img variant="top" src={googleLogo} />
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>{product.description}</Card.Text>
              <Card.Text>{product.price}$</Card.Text>
              <CardActions>
                <Fab
                  color="primary"
                  aria-label="add"
                  size="lg"
                  onClick={handleBuyClick}
                >
                  <Add />
                </Fab>
                <Button variant="secondary" size="lg" onClick={history.goBack}>
                  Back
                </Button>
              </CardActions>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default ProductDetail;
