import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setHeader } from "../../redux/reducer/headerReducer";
import ContentWithBorder from "../../component/Content/ContentWithBorder";

function Home() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setHeader("Home"));
  }, [dispatch]);

  return (
    <div>
      <ContentWithBorder>
        <h2>Home</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
          accumsan enim eleifend elit euismod, et rutrum lacus eleifend.
          Pellentesque sed egestas erat. Duis at sapien iaculis, porta nibh
          eget, lobortis nulla. Nullam tempor quis arcu in interdum. Aenean ut
          porttitor nunc, vitae dignissim mauris. Maecenas tincidunt sagittis
          dui, at auctor arcu tincidunt a. Suspendisse iaculis faucibus finibus.
          Aenean est nunc, lacinia et porta sit amet, mollis sit amet odio.
          Donec mollis libero ligula, quis sollicitudin quam pulvinar ut.
          Vivamus eget tempus lorem, in pellentesque risus. Integer in interdum
          metus. Morbi non leo interdum, congue erat vitae, tempor justo. Mauris
          laoreet sem non mi pulvinar interdum. Aliquam tellus magna, consequat
          nec mauris in, mattis laoreet libero. Ut cursus justo libero, nec
          pellentesque nunc ultrices in. Ut id aliquam ex.
        </p>

        <p>
          Nunc ipsum nunc, euismod sed justo id, placerat aliquet dolor. Nulla
          facilisi. Donec mollis tincidunt lacus, at consequat tortor aliquet
          vitae. In bibendum sed est quis porttitor. Aliquam vitae porta felis.
          Maecenas lacus nunc, imperdiet sed porta id, elementum ut augue.
          Aenean sit amet orci in velit viverra euismod vitae id sem. In
          scelerisque sem nec risus sollicitudin, sed auctor sapien rutrum.
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
          egestas luctus arcu a lobortis.
        </p>
      </ContentWithBorder>
    </div>
  );
}

export default Home;
