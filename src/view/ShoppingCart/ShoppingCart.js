import {
  Paper,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableBody,
} from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Fab } from "@mui/material";
import { Delete } from "@mui/icons-material";
import { shoppingCartAction } from "../../redux/action/shoppingCartAction";
import log from "loglevel";
import { setHeader } from "../../redux/reducer/headerReducer";
import Content from "../../component/Content/Content";
import { Button } from "react-bootstrap";
import { orderService } from "../../service/OrderService";

const HEADER = "Shopping Cart";

class ShoppingCart extends Component {
  componentDidMount() {
    this.props.dispatch(setHeader(HEADER));
  }

  getVat() {
    return this.props.totalVat / this.props.productCount;
  }

  getTax() {
    return (this.getVat() / 100) * this.props.totalPrice;
  }

  getSubtotal() {
    return this.props.totalPrice - this.getTax();
  }

  handleDeleteClick(product) {
    this.props.dispatch(shoppingCartAction.removeFromCart(product));
    log.info(
      "UserId: %d. Product with ID: %d has been removed from Shopping cart.",
      this.props.user.id,
      product.id
    );
  }

  handleOrderClick() {
    let shoppingCart = {
      products: this.props.products,
      totalPriceBrutto: this.props.totalPrice,
      totalPriceNetto: this.getSubtotal(),
    };
    orderService.addOrder(shoppingCart, this.props.user);
  }

  render() {
    let addedItems = this.props.products.length ? (
      this.props.products.map((product) => {
        return (
          <TableRow key={product.id}>
            <TableCell>{product.name}</TableCell>
            <TableCell>{product.quantity}</TableCell>
            <TableCell>{product.price}</TableCell>
            <TableCell>{product.price * product.quantity}</TableCell>
            <TableCell>
              <Fab
                color="default"
                aria-label="delete"
                size="small"
                onClick={() => this.handleDeleteClick(product)}
              >
                <Delete />
              </Fab>
            </TableCell>
          </TableRow>
        );
      })
    ) : (
      <TableRow>
        <TableCell align="center" colSpan={5}>
          Shopping Cart is empty
        </TableCell>
      </TableRow>
    );

    return (
      <Content>
        <TableContainer component={Paper} className="pb-5">
          <Table sx={{ minWidth: 700 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell>Unit price</TableCell>
                <TableCell>Sum</TableCell>
                <TableCell>Remove</TableCell>
              </TableRow>
            </TableHead>
            {addedItems && addedItems.length > 0 ? (
              <TableBody>
                {addedItems}
                <TableRow>
                  <TableCell colSpan={2}></TableCell>
                  <TableCell colSpan={2}>Subtotal</TableCell>
                  <TableCell>{this.getSubtotal()}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell colSpan={2}></TableCell>
                  <TableCell>Tax</TableCell>
                  <TableCell>{this.getVat()}%</TableCell>
                  <TableCell>{this.getTax()}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell colSpan={2}></TableCell>
                  <TableCell colSpan={2}>Total</TableCell>
                  <TableCell>{this.props.totalPrice}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell colSpan={4}></TableCell>
                  <TableCell>
                    <Button onClick={() => this.handleOrderClick()}>
                      Order
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            ) : (
              <TableBody>{addedItems}</TableBody>
            )}
          </Table>
        </TableContainer>
      </Content>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.shoppingCart.addedProducts,
    totalPrice: state.shoppingCart.total,
    totalVat: state.shoppingCart.totalVat,
    productCount: state.shoppingCart.productCount,
    user: state.userReducer.user,
  };
};

export default connect(mapStateToProps)(ShoppingCart);
