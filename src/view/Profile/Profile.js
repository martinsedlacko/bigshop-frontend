import React, { Component } from "react";
import { connect } from "react-redux";
import { setHeader } from "../../redux/reducer/headerReducer";
import "./Profile.css";

const HEADER = "Profile";

class Profile extends Component {
  componentDidMount() {
    this.props.setHeader();
  }

  render() {
    return (
      <div className="profile-info">
        <div className="profile-avatar">
          {this.props.authData.user.imageUrl ? (
            <img
              src={this.props.authData.user.imageUrl}
              alt={this.props.authData.user.name}
            />
          ) : (
            <div className="text-avatar">
              <span>
                {this.props.authData.user.name &&
                  this.props.authData.user.name[0]}
              </span>
            </div>
          )}
        </div>
        <div className="profile-name">
          <h2>{this.props.authData.user.name}</h2>
          <p className="profile-email">{this.props.authData.user.email}</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setHeader: () => dispatch(setHeader(HEADER)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
