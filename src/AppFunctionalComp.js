import React, { useEffect } from "react";
import LoadingIndicator from "./component/common/LoadingIndicator";
import { ACCESS_TOKEN } from "./constants";
import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import "./App.css";
import Layout from "./component/Layout/Layout";
import Routes from "./component/Routes";
import { userAction } from "./redux/action/userAction";
import { useSelector, useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const user = useSelector((state) => {
    return state.userReducer;
  });

  function handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    user.authenticated = false;
    user.user = null;
    Alert.success("You are safely logged out!");
  }

  useEffect(() => {
    dispatch(userAction.getCurrentUser());
  }, [dispatch]);

  if (user.isLoading) {
    return <LoadingIndicator />;
  } else {
    return (
      <div className="app">
        <Layout authData={user} logOut={handleLogout}>
          <Routes authData={user} />
        </Layout>
        <Alert
          stack={{ limit: 3 }}
          timeout={3000}
          position="top-right"
          effect="slide"
          offset={65}
        />
      </div>
    );
  }
}

export default App;
