import React, { useEffect } from "react";
import LoadingIndicator from "./component/common/LoadingIndicator";
import { ACCESS_TOKEN } from "./constants";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import "./App.css";
import Layout from "./component/Layout/Layout";
import Routes from "./component/Routes";
import { userAction } from "./redux/action/userAction";
import { useSelector, useDispatch } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import storage from "redux-persist/lib/storage";

function App() {
  const dispatch = useDispatch();
  const user = useSelector((state) => {
    return state.userReducer;
  });

  function handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    storage.removeItem("persist:root");
    user.authenticated = false;
    user.user = null;
  }

  useEffect(() => {
    dispatch(userAction.getCurrentUser());
  }, [dispatch]);

  if (user.isLoading) {
    return <LoadingIndicator />;
  } else {
    return (
      <div className="app">
        <Layout authData={user} logOut={handleLogout}>
          <ToastContainer
            position="top-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
          />
          <Routes authData={user} />
        </Layout>
      </div>
    );
  }
}

export default App;
