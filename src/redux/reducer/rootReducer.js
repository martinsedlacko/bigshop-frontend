import { combineReducers } from "redux";
import headerReducer from "./headerReducer";
import shoppingCartReducer from "./shoppingCartReducer";
import userReducer from "./userReducer";

const rootReducer = combineReducers({
  userReducer: userReducer,
  header: headerReducer,
  shoppingCart: shoppingCartReducer,
});

export default rootReducer;
