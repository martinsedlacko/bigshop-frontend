import { createSlice } from "@reduxjs/toolkit";

const headerReducer = createSlice({
  name: "header",
  initialState: {
    value: "Default Header",
  },
  reducers: {
    setHeader: (state, action) => {
      state.value = action.payload;
      Object.assign({}, state, { value: action.payload });
    },
  },
});

export const { setHeader } = headerReducer.actions;
export default headerReducer.reducer;
