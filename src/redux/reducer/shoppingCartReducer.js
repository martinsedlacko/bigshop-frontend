import shoppingCartConstant from "../constant/shoppingCartConstant";

const initState = {
  addedProducts: [],
  total: 0,
  totalVat: 0,
  productCount: 0,
};

const shoppingCartReducer = (state = initState, action) => {
  switch (action.type) {
    case shoppingCartConstant.ADD_TO_CART_SUCCESS: {
      let addedProduct = action.product;
      let itemAlreadyInCartIdx = state.addedProducts.findIndex(
        (product) => product.id === addedProduct.id
      );
      if (itemAlreadyInCartIdx !== -1) {
        return increateProductQuantity(
          state,
          itemAlreadyInCartIdx,
          addedProduct
        );
      }
      return addNewProduct(addedProduct, state);
    }
    case shoppingCartConstant.REMOVE_FROM_CART_SUCCESS: {
      let productToRemove = action.product;
      let itemInCartIdx = state.addedProducts.findIndex(
        (product) => product.id === productToRemove.id
      );
      if (itemInCartIdx !== -1) {
        let itemAlreadyInCart = state.addedProducts[itemInCartIdx];
        // create copy of all products
        let productsCopy = [...state.addedProducts];
        // remove product from copy of all products
        productsCopy.splice(itemInCartIdx, 1);

        // when itemToRemove has quantity > 1, then create copy of product
        // lower quantity by one and push to productsCopy
        // otherwise do not push product to productsCopy(product has been removed)
        if (itemAlreadyInCart.quantity > 1) {
          decreaseProductQuantity(
            productToRemove,
            itemAlreadyInCart,
            productsCopy
          );
        }
        return {
          ...state,
          addedProducts: productsCopy,
          total: state.total - itemAlreadyInCart.price,
          totalVat: state.totalVat - itemAlreadyInCart.vat,
          productCount: state.productCount - 1,
        };
      }
      break;
    }
    default:
      return state;
  }
};

function increateProductQuantity(state, itemAlreadyInCartIdx, addedProduct) {
  let itemAlreadyInCart = state.addedProducts[itemAlreadyInCartIdx];
  let productsCopy = [...state.addedProducts];
  productsCopy.splice(itemAlreadyInCartIdx, 1);

  let addedProductCopy = Object.assign({}, addedProduct);
  addedProductCopy.quantity = itemAlreadyInCart.quantity + 1;

  productsCopy.push(addedProductCopy);
  return {
    ...state,
    addedProducts: productsCopy,
    total: state.total + itemAlreadyInCart.price,
    totalVat: state.totalVat + itemAlreadyInCart.vat,
    productCount: state.productCount + 1,
  };
}

function addNewProduct(addedProduct, state) {
  addedProduct.quantity = 1;
  return {
    ...state,
    addedProducts: [...state.addedProducts, addedProduct],
    total: state.total + addedProduct.price,
    totalVat: state.totalVat + addedProduct.vat,
    productCount: state.productCount + 1,
  };
}

function decreaseProductQuantity(
  productToRemove,
  itemAlreadyInCart,
  productsCopy
) {
  let productToRemoveCopy = Object.assign({}, productToRemove);
  productToRemoveCopy.quantity = itemAlreadyInCart.quantity - 1;
  productsCopy.push(productToRemoveCopy);
}

export default shoppingCartReducer;
