import userConstant from "../constant/userConstant";

const userReducer = (state, action) => {
  switch (action.type) {
    case userConstant.ADD_USER: {
      return {
        ...state,
        user: action.user,
      };
    }
    case userConstant.GET_CURRENT_USER_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case userConstant.GET_CURRENT_USER_SUCCESS: {
      return {
        ...state,
        user: action.user,
        isLoading: false,
        authenticated: true,
      };
    }
    case userConstant.GET_CURRENT_USER_ERROR: {
      return {
        ...state,
        user: null,
        isLoading: false,
        error: action.error,
        authenticated: false,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default userReducer;
