import { userConstant } from "../constant/userConstant";
import { userService } from "../../service/UserService";

export const userAction = {
  addUser,
  getCurrentUser,
};

function addUser(user) {
  return (dispatch) => {
    dispatch(addUser(user));
  };

  function addUser(user) {
    return {
      type: userConstant.ADD_USER,
      user: user,
    };
  }
}

function getCurrentUser() {
  return (dispatch) => {
    dispatch(request());

    userService.getCurrentUser().then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: userConstant.GET_CURRENT_USER_REQUEST };
  }

  function success(user) {
    return { type: userConstant.GET_CURRENT_USER_SUCCESS, user };
  }

  function failure(error) {
    return { type: userConstant.GET_CURRENT_USER_ERROR, error };
  }
}
