import shoppingCartConstant from "../constant/shoppingCartConstant";

export const shoppingCartAction = {
  addToCart,
  removeFromCart,
};

function addToCart(product) {
  return (dispatch) => dispatch(success(product));

  function success(product) {
    return { type: shoppingCartConstant.ADD_TO_CART_SUCCESS, product };
  }
}

function removeFromCart(product) {
  return (dispatch) => dispatch(success(product));

  function success(product) {
    return { type: shoppingCartConstant.REMOVE_FROM_CART_SUCCESS, product };
  }
}
