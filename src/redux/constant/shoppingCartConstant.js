export const shoppingCartConstant = {
  ADD_TO_CART_SUCCESS: "ADD_TO_CART_SUCCESS",
  REMOVE_FROM_CART_SUCCESS: "REMOVE_FROM_CART_SUCCESS",
};

export default shoppingCartConstant;
