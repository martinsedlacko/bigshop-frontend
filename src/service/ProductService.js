import { API_BASE_URL, ACCESS_TOKEN } from "../constants";
import { request } from "../util/APIUtils";

export const productService = {
  getAllProducts,
};

function getAllProducts() {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }

  return request({
    url: API_BASE_URL + "/product/findAll",
    method: "GET",
  });
}
