import { API_BASE_URL, ACCESS_TOKEN } from "../constants";
import { request } from "../util/APIUtils";

export const userService = {
  getCurrentUser,
};

function getCurrentUser() {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }

  return request({
    url: API_BASE_URL + "/user/me",
    method: "GET",
  });
}
