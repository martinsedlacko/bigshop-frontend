import { API_BASE_URL, ACCESS_TOKEN } from "../constants";
import { getHeaders } from "../util/APIUtils";
import axios from "axios";

export const orderService = {
  addOrder,
};

function addOrder(shoppingCart, client) {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }

  axios.post(
    API_BASE_URL + "/order/add",
    {
      client: client,
      shoppingCart: shoppingCart,
    },
    { headers: getHeaders() }
  );
}
