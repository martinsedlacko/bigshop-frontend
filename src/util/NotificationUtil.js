import { toast } from "react-toastify";

export const NotificationUtil = {
  success,
  error,
  warn,
  info,
};

const options = {
  position: "top-center",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

function success(message) {
  toast.success(message, options);
}

function error(message) {
  toast.error(message, options);
}

function warn(message) {
  toast.warn(message, options);
}

function info(message) {
  toast.info(message, options);
}
